package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.repository.CardRepository;
import employeetracking.aluminc.util.Util;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
public class CardControllerTest extends BaseTest {

    private static final String API_CARD = "/card";


    @Autowired
    private CardRepository cardRepository;

    @Test
    public void createCardTest() throws Exception {

        Card card = new Card("11111","4444", Timestamp.valueOf("2018-01-01 11:11:00"),"0");

        //when
        this.mockMvc.perform(post(API_CARD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(card)))

                //then
                .andExpect(jsonPath("$.rfId", Matchers.is("11111")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void getCardTest() throws Exception {
        Card card = new Card("11111","4444", Timestamp.valueOf("2018-01-01 11:11:00"),"0");
        cardRepository.save(card);

        //when
        this.mockMvc.perform(get(API_CARD + "/" + card.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(card)))

                //then
                .andExpect(jsonPath("$.rfId", Matchers.is("11111")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllCardTest() throws Exception {

        //when
        this.mockMvc.perform(get(API_CARD + "/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateCardTest() throws Exception {
        Card card = new Card("11111","4444", Timestamp.valueOf("2018-01-01 11:11:00"),"0");
        cardRepository.save(card);

        Card card2 = new Card("11112","4444", Timestamp.valueOf("2018-01-01 11:11:00"),"0");

        this.mockMvc.perform(put(API_CARD + "/" + card.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(card2)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.rfId", Matchers.is("11112")))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCardTest() throws Exception {
        Card card = new Card("11111","4444", Timestamp.valueOf("2018-01-01 11:11:00"),"0");
        cardRepository.save(card);

        this.mockMvc.perform(delete(API_CARD + "/" + card.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(card)))
                .andExpect(status().isOk());
    }


}