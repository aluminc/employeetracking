package employeetracking.aluminc.controller;

import employeetracking.aluminc.Application;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class BaseTest {

    protected MockMvc mockMvc;
    @Autowired
    protected WebApplicationContext webApplicationContext;

    public BaseTest() {
    }

    @Before
    public void before() throws IOException {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }
}
