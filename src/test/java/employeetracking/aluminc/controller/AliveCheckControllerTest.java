package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.AliveCheck;
import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.repository.AliveCheckRepository;
import employeetracking.aluminc.util.Util;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
public class AliveCheckControllerTest extends BaseTest {

    private static final String API_CARD = "/aliveCheck";


    @Autowired
    private AliveCheckRepository aliveCheckRepository;

    @Test
    public void createAliveCheckTest() throws Exception {

        AliveCheck aliveCheck = new AliveCheck(0,"000000000001","20.0C", "21.5C", Timestamp.valueOf("2018-01-01 11:11:00"),0);

        //when
        this.mockMvc.perform(post(API_CARD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(aliveCheck)))

                //then
                .andExpect(jsonPath("$.cpuId", Matchers.is("000000000001")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void getAliveCheckTest() throws Exception {
        AliveCheck aliveCheck = new AliveCheck(0,"000000000001","20.0C", "21.5C", Timestamp.valueOf("2018-01-01 11:11:00"),0);
        aliveCheckRepository.save(aliveCheck);

        //when
        this.mockMvc.perform(get(API_CARD + "/" + aliveCheck.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(aliveCheck)))

                //then
                .andExpect(jsonPath("$.cpuId", Matchers.is("000000000001")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllAliveCheckTest() throws Exception {

        //when
        this.mockMvc.perform(get(API_CARD + "/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateAliveCheckTest() throws Exception {
        AliveCheck aliveCheck = new AliveCheck(0,"000000000001","20.0C", "21.5C", Timestamp.valueOf("2018-01-01 11:11:00"),0);
        aliveCheckRepository.save(aliveCheck);

        AliveCheck aliveCheck2 = new AliveCheck(0,"000000000001","21.0C", "21.5C", Timestamp.valueOf("2018-01-01 11:11:00"),0);

        this.mockMvc.perform(put(API_CARD + "/" + aliveCheck.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(aliveCheck2)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.cpuTemp", Matchers.is("21.0C")))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteAliveCheckTest() throws Exception {
        AliveCheck aliveCheck = new AliveCheck(0,"000000000001","20.0C", "21.5C", Timestamp.valueOf("2018-01-01 11:11:00"),0);
        aliveCheckRepository.save(aliveCheck);

        this.mockMvc.perform(delete(API_CARD + "/" + aliveCheck.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(aliveCheck)))
                .andExpect(status().isOk());
    }


}