package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Device;
import employeetracking.aluminc.repository.DeviceRepository;
import employeetracking.aluminc.util.Util;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
public class DeviceControllerTest extends BaseTest {

    private static final String API_DEVICE = "/device";


    @Autowired
    private DeviceRepository deviceRepository;

    @Test
    public void createDeviceTest() throws Exception {

        Device device = new Device("11111",0, 0, Timestamp.valueOf("2018-01-01 11:11:00"));

        //when
        this.mockMvc.perform(post(API_DEVICE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(device)))

                //then
                .andExpect(jsonPath("$.cpuId", Matchers.is("11111")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void getDeviceTest() throws Exception {
        Device device = new Device("11111",0, 0, Timestamp.valueOf("2018-01-01 11:11:00"));
        deviceRepository.save(device);

        //when
        this.mockMvc.perform(get(API_DEVICE + "/" + device.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(device)))

                //then
                .andExpect(jsonPath("$.cpuId", Matchers.is("11111")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllDeviceTest() throws Exception {

        //when
        this.mockMvc.perform(get(API_DEVICE + "/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateDeviceTest() throws Exception {
        Device device = new Device("11111",0, 0, Timestamp.valueOf("2018-01-01 11:11:00"));
        deviceRepository.save(device);

        Device device2 = new Device("11112",0, 0, Timestamp.valueOf("2018-01-01 11:11:00"));

        this.mockMvc.perform(put(API_DEVICE + "/" + device.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(device2)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.cpuId", Matchers.is("11112")))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteDeviceTest() throws Exception {
        Device device = new Device("11111",0, 0, Timestamp.valueOf("2018-01-01 11:11:00"));
        deviceRepository.save(device);

        this.mockMvc.perform(delete(API_DEVICE + "/" + device.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(device)))
                .andExpect(status().isOk());
    }


}