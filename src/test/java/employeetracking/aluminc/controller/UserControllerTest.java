package employeetracking.aluminc.controller;

import employeetracking.aluminc.repository.UserRepository;
import employeetracking.aluminc.model.User;
import employeetracking.aluminc.util.Util;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
public class UserControllerTest extends BaseTest {

    private static final String API_USER = "/user";


    @Autowired
    private UserRepository userRepository;

    @Test
    public void createUserTest() throws Exception {

        User user = new User("Full Name","email@email", 0, 0,"pass");

        //when
        this.mockMvc.perform(post(API_USER)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(user)))

                //then
                .andExpect(jsonPath("$.fullName", Matchers.is("Full Name")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void getUserTest() throws Exception {
        User user = new User("Full Name","email@email", 0, 0,"pass");
        userRepository.save(user);

        //when
        this.mockMvc.perform(get(API_USER + "/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(user)))

                //then
                .andExpect(jsonPath("$.fullName", Matchers.is("Full Name")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllUserTest() throws Exception {

        //when
        this.mockMvc.perform(get(API_USER + "/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUserTest() throws Exception {
        User user = new User("Full Name","email@email", 0, 0,"pass");
        userRepository.save(user);

        User user2 = new User("Full New Name","email@email", 0, 0,"pass");

        this.mockMvc.perform(put(API_USER + "/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(user2)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.fullName", Matchers.is("Full New Name")))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCardTest() throws Exception {
        User user = new User("Full Name","email@email", 0, 0,"pass");
        userRepository.save(user);

        this.mockMvc.perform(delete(API_USER + "/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(user)))
                .andExpect(status().isOk());
    }


}