package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Company;
import employeetracking.aluminc.repository.CompanyRepository;
import employeetracking.aluminc.util.Util;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
public class CompanyControllerTest extends BaseTest {

    private static final String API_COMPANY = "/company";


    @Autowired
    private CompanyRepository companyRepository;

    @Test
    public void createCompanyTest() throws Exception {

        Company company = new Company("Test Company","email@company.com", "5555555","adress", "contact person", "5555555556", "", "22222");

        //when
        this.mockMvc.perform(post(API_COMPANY)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(company)))

                //then
                .andExpect(jsonPath("$.companyName", Matchers.is("Test Company")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void getCompanyTest() throws Exception {
        Company company = new Company("Test Company","email@company.com", "5555555","adress", "contact person", "5555555556", "", "22222");
        companyRepository.save(company);

        //when
        this.mockMvc.perform(get(API_COMPANY + "/" + company.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(company)))

                //then
                .andExpect(jsonPath("$.companyName", Matchers.is("Test Company")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllCompanyTest() throws Exception {

        //when
        this.mockMvc.perform(get(API_COMPANY + "/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateCompanyest() throws Exception {
        Company company = new Company("Test Company","email@company.com", "5555555","adress", "contact person", "5555555556", "", "22222");
        companyRepository.save(company);

        Company company2 = new Company("Test Company new","email@company.com", "5555555","adress", "contact person", "5555555556", "", "22222");

        this.mockMvc.perform(put(API_COMPANY + "/" + company.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(company2)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.companyName", Matchers.is("Test Company new")))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCompanyTest() throws Exception {
        Company company = new Company("Test Company","email@company.com", "5555555","adress", "contact person", "5555555556", "", "22222");
        companyRepository.save(company);

        this.mockMvc.perform(delete(API_COMPANY + "/" + company.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(company)))
                .andExpect(status().isOk());
    }


}