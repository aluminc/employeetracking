package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Employee;
import employeetracking.aluminc.repository.EmployeeRepository;
import employeetracking.aluminc.util.Util;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
public class EmployeeControllerTest extends BaseTest {

    private static final String API_EMPLOYEE = "/employee";


    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void createEmployeeTest() throws Exception {

        Employee employee = new Employee("First", "Middle", "Last","email@email", "5555555", 0, 0, "");

        //when
        this.mockMvc.perform(post(API_EMPLOYEE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(employee)))

                //then
                .andExpect(jsonPath("$.firstName", Matchers.is("First")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void getEmployeeTest() throws Exception {
        Employee employee = new Employee("First", "Middle", "Last","email@email", "5555555", 0, 0, "");
        employeeRepository.save(employee);

        //when
        this.mockMvc.perform(get(API_EMPLOYEE + "/" + employee.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(employee)))

                //then
                .andExpect(jsonPath("$.firstName", Matchers.is("First")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllEmployeeTest() throws Exception {

        //when
        this.mockMvc.perform(get(API_EMPLOYEE + "/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateEmployeeTest() throws Exception {
        Employee employee = new Employee("First", "Middle", "Last","email@email", "5555555", 0, 0, "");
        employeeRepository.save(employee);

        Employee employee2 = new Employee("First2", "Middle", "Last","email@email", "5555555", 0, 0, "");

        this.mockMvc.perform(put(API_EMPLOYEE + "/" + employee.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(employee2)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.firstName", Matchers.is("First2")))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCardTest() throws Exception {
        Employee employee = new Employee("First", "Middle", "Last","email@email", "5555555", 0, 0, "");
        employeeRepository.save(employee);

        this.mockMvc.perform(delete(API_EMPLOYEE + "/" + employee.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(employee)))
                .andExpect(status().isOk());
    }


}