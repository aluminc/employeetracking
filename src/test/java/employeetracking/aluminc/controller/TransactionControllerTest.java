package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Transaction;
import employeetracking.aluminc.repository.TransactionRepository;
import employeetracking.aluminc.util.Util;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
public class TransactionControllerTest extends BaseTest {

    private static final String API_TRANSACTION = "/transaction";


    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void createTransactionTest() throws Exception {

        Transaction transaction = new Transaction(0, 0, "11111", 0, "4444", 0, Timestamp.valueOf("2018-01-01 11:11:00"),"IN", 0, "0/0");

        //when
        this.mockMvc.perform(post(API_TRANSACTION)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(transaction)))

                //then
                .andExpect(jsonPath("$.cpuId", Matchers.is("11111")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void getTransactionTest() throws Exception {
        Transaction transaction = new Transaction(0, 0, "11111", 0, "4444", 0, Timestamp.valueOf("2018-01-01 11:11:00"),"IN", 0, "0/0");
        transactionRepository.save(transaction);

        //when
        this.mockMvc.perform(get(API_TRANSACTION + "/" + transaction.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(transaction)))

                //then
                .andExpect(jsonPath("$.cpuId", Matchers.is("11111")))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllTransactionTest() throws Exception {

        //when
        this.mockMvc.perform(get(API_TRANSACTION + "/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void updateTransactionTest() throws Exception {
        Transaction transaction = new Transaction(0, 0, "11111", 0, "4444", 0, Timestamp.valueOf("2018-01-01 11:11:00"),"IN", 0, "0/0");
        transactionRepository.save(transaction);

        Transaction transaction2 = new Transaction(0, 0, "11112", 0, "4444", 0, Timestamp.valueOf("2018-01-01 11:11:00"),"IN", 0, "0/0");

        this.mockMvc.perform(put(API_TRANSACTION + "/" + transaction.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(transaction2)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.cpuId", Matchers.is("11112")))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTransactionTest() throws Exception {
        Transaction transaction = new Transaction(0, 0, "11111", 0, "4444", 0, Timestamp.valueOf("2018-01-01 11:11:00"),"IN", 0, "0/0");
        transactionRepository.save(transaction);

        this.mockMvc.perform(delete(API_TRANSACTION + "/" + transaction.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .content(Util.jsonString(transaction)))
                .andExpect(status().isOk());
    }


}