package employeetracking.aluminc.util;

import org.springframework.http.HttpStatus;

/**
 * Error codes for communicating errors to an HTTP client. The name of the enum variable (e.g. UNEXPECTED_ERROR) is
 * communicated to the client as a string. Changing either the name or the HTTP status code may break a client.
 */
public enum ErrorCode {
    /**
     * Unexpected error is used in a catch-all exception handler for errors that should not normally occur.
     */

    NOT_FOUND(HttpStatus.NOT_FOUND),
    UNEXPECTED_ERROR(HttpStatus.INTERNAL_SERVER_ERROR),
    CONSTRAINT_ERROR(HttpStatus.CONFLICT),;


    public final HttpStatus status;

    ErrorCode(HttpStatus status) {
        this.status = status;
    }


}
