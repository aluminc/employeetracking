package employeetracking.aluminc.util;

import employeetracking.aluminc.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class EmployeeTrackingExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ApplicationException.class);

    @ExceptionHandler(ApplicationException.class)
    @ResponseBody
    public ResponseEntity<ApplicationException> handleException(ApplicationException e) {
        if (e.getErrorCode().status.is4xxClientError()) {
            // less-serious logging & level for user-generated errors
            logger.error("{}: {}", e.getErrorCode().name(), e.getMessage());
        } else {
            logger.error("Sending error response to HTTP client: {}", e.getMessage(), e);
        }
        return new ResponseEntity<>(e, e.getErrorCode().status);
    }

    @ExceptionHandler
    public ResponseEntity<ApplicationException> handleGenericException(Exception e) {
        return new ResponseEntity<>(new ApplicationException(e.getMessage(), ErrorCode.UNEXPECTED_ERROR, e),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
