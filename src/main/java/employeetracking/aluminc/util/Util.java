package employeetracking.aluminc.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;

public class Util {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.findAndRegisterModules();
        objectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
        objectMapper.configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }


    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static <T> String jsonString(T object) {
        if (object instanceof Exception) {
            Exception exc = (Exception) object;
            return jsonString(ImmutableMap.of(
                    "message", Optional.ofNullable(exc.getMessage()),
                    "errorCode", "1"
            ));
        }
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    public static String rightsCompare(String first, String second) {
        HashMap<String, Integer> rights = new HashMap<String, Integer>();

        rights.put("N", 0);
        rights.put("R(", 1);
        rights.put("C", 2);
        rights.put("RC", 3);
        rights.put("O", 4);
        rights.put("V", 8);
        rights.put("RCV", 11);
        rights.put("W", 16);
        rights.put("RW", 17);
        rights.put("RCW", 19);
        rights.put("RCVW", 27);
        rights.put("P", 32);
        rights.put("RCOWP", 55);
        rights.put("RCVWP", 59);


        return rights.get(first) >= rights.get(second) ? first : second;

    }

    public static <T> T parseJson(String json, Class<T> resultType) {
        try {
            return objectMapper.readValue(json, resultType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Same as JavaScript's Object.assign(), except this one doesn't modify its inputs.
     */
    public static <K, V> ImmutableMap<K, V> assign(ImmutableMap<K, V>... maps) {
        HashMap<K, V> dst = new HashMap<>();
        for (ImmutableMap<K, V> src : maps) dst.putAll(src); // Replaces duplicate keys, unlike ImmutableMap.Builder.
        return ImmutableMap.copyOf(dst);
    }

    /**
     * Map keys and values of a map using the given functions. It is an error for there to be duplicate keys.
     */
    public static <K1, V1, K2, V2> ImmutableMap<K2, V2> map
    (ImmutableMap<K1, V1> src, Function<K1, K2> mapKey, Function<V1, V2> mapValue) {
        ImmutableMap.Builder<K2, V2> builder = ImmutableMap.builder();
        src.forEach((key, value) -> builder.put(mapKey.apply(key), mapValue.apply(value)));
        return builder.build();
    }

    /**
     * Update pojo with map values. Iterate through pojo fields and trying to find matching map key values and pojo fields,
     * found pojo fields will be updated with corresponding map value.
     *
     * @param pojo  - pojo to update.
     * @param map   - map of key, values to update pojo with.
     * @param clazz - pojo class that iterate through and find corresponding fields for update its/their value.
     * @param <T>   - any pojo object
     */
    public static <T> void updatePojoByMap(T pojo, Map<String, Object> map, Class<?> clazz) {
        if (clazz != null) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                //static or final, then we do not want to copy its value
                if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isFinal(field.getModifiers())
                        && map.containsKey(field.getName())) {

                    // The field might be inaccessible, so let's force it to be accessible.
                    field.setAccessible(true);
                    try {
                        //checks is field enum
                        if (field.getType().isEnum()) {
                            field.set(pojo, Enum.valueOf((Class<Enum>) field.getType(), String.valueOf(map.get(field.getName()))));
                        } else if (field.getType().isAssignableFrom(Instant.class)) {
                            Instant timeValue = map.get(field.getName()) != null ? Instant.parse(map.get(field.getName()).toString()) : null;
                            field.set(pojo, timeValue);
                        } else if (field.getType().isAssignableFrom(BigDecimal.class)) {
                            field.set(pojo, new BigDecimal(map.get(field.getName()).toString()));
                        } else {
                            field.set(pojo, map.get(field.getName()));
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            // Do the same copying of fields to this class's superclass.
            updatePojoByMap(pojo, map, clazz.getSuperclass());
        }
    }

    public static <T> void copyPojoToPojo(T toPojo, T fromPojo, Class<?> clazz) {
        if (clazz != null) {
            Arrays.asList(clazz.getDeclaredFields()).forEach(f -> {
                try {
                    f.setAccessible(true);
                    f.set(toPojo, f.get(fromPojo));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
            copyPojoToPojo(toPojo, fromPojo, clazz.getSuperclass());
        }
    }

}
