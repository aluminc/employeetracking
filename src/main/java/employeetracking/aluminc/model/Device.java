package employeetracking.aluminc.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="device")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String cpuId;

    private Integer companyId;

    private Integer status;

    private Timestamp lastLogTime;

    public Device(){}

    public Device(String cpuId, int companyId, int status, Timestamp lastLogTime) {
        this.cpuId = cpuId;
        this.companyId = companyId;
        this.status = status;
        this.lastLogTime = lastLogTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpuId() {
        return cpuId;
    }

    public void setCpuId(String cpuId) {
        this.cpuId = cpuId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getLastLogTime() {
        return lastLogTime;
    }

    public void setLastLogTime(Timestamp lastLogTime) {
        this.lastLogTime = lastLogTime;
    }
}

