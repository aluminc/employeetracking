package employeetracking.aluminc.model;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name="transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer companyId;

    private Integer deviceId;

    private String cpuId;

    private Integer cardId;

    private String rfId;

    private Integer employeeId;

    private Timestamp dateTime;

    private String transactionType;

    private Integer status;

    private String logNum;


    public Transaction() {}

    public Transaction(int companyId, int deviceId, String cpuId, int cardId, String rfId, int employeeId, Timestamp dateTime, String transactionType, int status, String logNum) {
        this.companyId = companyId;
        this.deviceId = deviceId;
        this.cpuId = cpuId;
        this.cardId = cardId;
        this.rfId = rfId;
        this.employeeId = employeeId;
        this.dateTime = dateTime;
        this.transactionType = transactionType;
        this.status = status;
        this.logNum = logNum;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getCpuId() {
        return cpuId;
    }

    public void setCpuId(String cpuId) {
        this.cpuId = cpuId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getRfId() {
        return rfId;
    }

    public void setRfId(String rfId) {
        this.rfId = rfId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLogNum() {
        return logNum;
    }

    public void setLogNum(String logNum) {
        this.logNum = logNum;
    }
}

