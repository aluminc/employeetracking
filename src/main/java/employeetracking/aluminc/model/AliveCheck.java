package employeetracking.aluminc.model;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name="alive_check")
public class AliveCheck {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer deviceId;

    private String cpuId;

    private String cpuTemp;

    private Timestamp dateTime;

    private String ambTemp;

    private Integer status;


    public AliveCheck() {}

    public AliveCheck( int deviceId, String cpuId, String cpuTemp, String ambTemp, Timestamp dateTime, int status) {
        this.deviceId = deviceId;
        this.cpuId = cpuId;
        this.cpuTemp = cpuTemp;
        this.dateTime = dateTime;
        this.ambTemp = ambTemp;
        this.status = status;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getCpuId() {
        return cpuId;
    }

    public void setCpuId(String cpuId) {
        this.cpuId = cpuId;
    }

    public String getCpuTemp() {
        return cpuTemp;
    }

    public void setCpuTemp(String cpuTemp) {
        this.cpuTemp = cpuTemp;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getAmbTemp() {
        return ambTemp;
    }

    public void setAmbTemp(String ambTemp) {
        this.ambTemp = ambTemp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}

