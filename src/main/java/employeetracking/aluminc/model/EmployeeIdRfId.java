package employeetracking.aluminc.model;


public class EmployeeIdRfId {

    private Integer employeeId;

    private String rfId;


    public EmployeeIdRfId() {}

    public EmployeeIdRfId(int employeeId, String rfId) {
        this.rfId = rfId;
        this.employeeId = employeeId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getRfId() {
        return rfId;
    }

    public void setRfId(String rfId) {
        this.rfId = rfId;
    }
}

