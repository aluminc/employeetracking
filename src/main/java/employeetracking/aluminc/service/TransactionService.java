package employeetracking.aluminc.service;

import employeetracking.aluminc.exception.ApplicationException;
import employeetracking.aluminc.model.Transaction;
import employeetracking.aluminc.model.Device;
import employeetracking.aluminc.repository.DeviceRepository;
import employeetracking.aluminc.repository.TransactionRepository;
import employeetracking.aluminc.util.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class TransactionService {

    private static Logger logger = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    DeviceService deviceService;

    @Autowired
    CardService cardService;

    public Transaction create(Transaction transaction) {
        transaction.setDeviceId(deviceService.getId(transaction.getCpuId()));
        transaction.setCompanyId(deviceService.getCompanyId(transaction.getCpuId()));
        transaction.setCardId(cardService.getId(transaction.getRfId()));
        transactionRepository.save(transaction);
        logger.debug("Created Transaction {} ...", transaction.getCpuId() + " - " + transaction.getRfId());
        return transaction;
    }

    public Transaction getTransaction(int id) {
        Transaction transaction = transactionRepository.findById(id).orElseThrow(() -> new ApplicationException("Transaction not found", ErrorCode.NOT_FOUND));
        logger.debug("Get Transaction {} ...", transaction.getCpuId() + " - " + transaction.getRfId());
        return transaction;
    }

    public List<Transaction> getAll() {
        List<Transaction> transactionList = transactionRepository.findAll();
        return transactionList;
    }

    public Transaction updateTransaction(Transaction transaction){
        logger.debug("Updating Transaction {} ...", transaction.getCpuId() + " - " + transaction.getRfId());
        transactionRepository.save(transaction);
        logger.debug("transaction Updated{} ...", transaction.getCpuId() + " - " + transaction.getRfId());
        return transaction;
    }

    public String deleteTransaction(int id) {
        Transaction transaction = transactionRepository.findById(id).orElseThrow(() -> new ApplicationException("Transaction not found", ErrorCode.NOT_FOUND));
        try {

            transactionRepository.delete(getTransaction(id));
        }
        catch (Exception DataIntegrityViolationException)
        {
            return "must return";
        }
        return "must return";
    }

}
