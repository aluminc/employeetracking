package employeetracking.aluminc.service;

import employeetracking.aluminc.exception.ApplicationException;
import employeetracking.aluminc.model.Employee;
import employeetracking.aluminc.model.Device;
import employeetracking.aluminc.model.EmployeeIdRfId;
import employeetracking.aluminc.repository.DeviceRepository;
import employeetracking.aluminc.repository.EmployeeRepository;
import employeetracking.aluminc.util.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class EmployeeService {

    private static Logger logger = LoggerFactory.getLogger(EmployeeService.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private CardService cardService;

    public Employee create(Employee employee) {
        logger.debug("Creating Employee {} ...", employee.getFirstName()+" "+employee.getLastName());
        employeeRepository.save(employee);
        logger.debug("Created Employee {} ...", employee.getFirstName()+" "+employee.getLastName());
        return employee;
    }

    public Employee getEmployee(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new ApplicationException("Employee not found", ErrorCode.NOT_FOUND));
        logger.debug("Get Employee {} ...", employee.getFirstName()+" "+employee.getLastName());
        return employee;
    }

    public List<Employee> getAll() {
        List<Employee> employeeList = employeeRepository.findAll();
        return employeeList;
    }

    public List<EmployeeIdRfId> getAllByDevice(String cpuId) {
        Device device = deviceRepository.findByCpuId(cpuId).orElseThrow(() -> new ApplicationException("Device not found", ErrorCode.NOT_FOUND));
        List<Employee> employeeList = employeeRepository.findAllByCompanyId(device.getCompanyId());
        List<EmployeeIdRfId> employeeIdRfIds = new ArrayList<EmployeeIdRfId>();
        for(Employee employee:employeeList){
            EmployeeIdRfId employeeIdRfId = new EmployeeIdRfId(employee.getId(), cardService.getCard(employee.getCardId()).getRfId());
            employeeIdRfIds.add(employeeIdRfId);
        }
        return employeeIdRfIds;
    }

    public Employee updateEmployee(Employee employee){
        logger.debug("Updating Employee {} ...", employee.getFirstName()+" "+employee.getLastName());
        Employee oldEmployee = employeeRepository.findById(employee.getId()).orElseThrow(() -> new ApplicationException("Employee not found", ErrorCode.NOT_FOUND));
        employeeRepository.save(employee);
        logger.debug("Updating Employee {} ...", employee.getFirstName()+" "+employee.getLastName());
        return employee;
    }

    public String deleteEmployee(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new ApplicationException("Employee not found", ErrorCode.NOT_FOUND));
        try {

            employeeRepository.delete(getEmployee(id));
        }
        catch (Exception DataIntegrityViolationException){
            return "must return";
        }
        return "must return";
    }

}
