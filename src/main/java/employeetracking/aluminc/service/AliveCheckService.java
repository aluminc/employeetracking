package employeetracking.aluminc.service;

import employeetracking.aluminc.exception.ApplicationException;
import employeetracking.aluminc.model.AliveCheck;
import employeetracking.aluminc.repository.AliveCheckRepository;
import employeetracking.aluminc.util.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AliveCheckService {

    private static Logger logger = LoggerFactory.getLogger(AliveCheckService.class);

    @Autowired
    private AliveCheckRepository aliveCheckRepository;

    @Autowired
    DeviceService deviceService;

    public AliveCheck create(AliveCheck aliveCheck) {
        logger.debug("Creating AliveCheck {} ...", aliveCheck.getCpuId());
        aliveCheck.setDeviceId(deviceService.getId(aliveCheck.getCpuId()));
        aliveCheckRepository.save(aliveCheck);
        logger.debug("Created AliveCheck {}", aliveCheck.getCpuId());
        return aliveCheck;
    }

    public AliveCheck getAliveCheck(int id) {
        AliveCheck aliveCheck = aliveCheckRepository.findById(id).orElseThrow(() -> new ApplicationException("AliveCheck not found", ErrorCode.NOT_FOUND));
        logger.debug("Get AliveCheck {} ...", aliveCheck.getCpuId());
        return aliveCheck;
    }

    public List<AliveCheck> getAll() {
        List<AliveCheck> aliveCheckList = aliveCheckRepository.findAll();
        return aliveCheckList;
    }

    public AliveCheck updateAliveCheck(AliveCheck aliveCheck){
        logger.debug("Updating AliveCheck {} ...", aliveCheck.getCpuId());
        AliveCheck oldAliveCheck = aliveCheckRepository.findById(aliveCheck.getId()).orElseThrow(() -> new ApplicationException("AliveCheck not found", ErrorCode.NOT_FOUND));
        aliveCheckRepository.save(aliveCheck);
        logger.debug("AliveCheck Updated{} ...", aliveCheck.getCpuId());
        return aliveCheck;
    }

    public String deleteAliveCheck(int id) {
        AliveCheck depot = aliveCheckRepository.findById(id).orElseThrow(() -> new ApplicationException("AliveCheck not found", ErrorCode.NOT_FOUND));
        try {

            aliveCheckRepository.delete(getAliveCheck(id));
        }
        catch (Exception DataIntegrityViolationException)
        {  // logger.debug("Please Remove Groups of Depot");
            return "must return";
        }
       // logger.debug("Depot deleted {} ...", depot.toString());
        return "must return";
    }

}
