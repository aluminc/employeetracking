package employeetracking.aluminc.service;

import employeetracking.aluminc.exception.ApplicationException;
import employeetracking.aluminc.model.Company;
import employeetracking.aluminc.repository.CompanyRepository;
import employeetracking.aluminc.util.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class CompanyService {

    private static Logger logger = LoggerFactory.getLogger(CompanyService.class);

    @Autowired
    private CompanyRepository companyRepository;

    public Company create(Company company) {
        logger.debug("Creating Company {} ...", company.getCompanyName());
        companyRepository.save(company);
        logger.debug("Created Company {} ...", company.getCompanyName());
        return company;
    }

    public Company getCompany(int id) {
        Company company = companyRepository.findById(id).orElseThrow(() -> new ApplicationException("Company not found", ErrorCode.NOT_FOUND));
        logger.debug("Get Company {} ...", company.getCompanyName());
        return company;
    }

    public List<Company> getAll() {
        List<Company> commapnyList = companyRepository.findAll();
        return commapnyList;
    }

    public Company updateCompany(Company company){
        logger.debug("Updating Compoany {} ...", company.getCompanyName());
        Company oldCompany = companyRepository.findById(company.getId()).orElseThrow(() -> new ApplicationException("Company not found", ErrorCode.NOT_FOUND));
        companyRepository.save(company);
        logger.debug("Updated Compoany {} ...", company.getCompanyName());
        return company;
    }

    public String deleteCompany(int id) {
        Company company = companyRepository.findById(id).orElseThrow(() -> new ApplicationException("Company not found", ErrorCode.NOT_FOUND));
        try {

            companyRepository.delete(getCompany(id));
        }
        catch (Exception DataIntegrityViolationException){
            return "must return";
        }
        return "must return";
    }

}
