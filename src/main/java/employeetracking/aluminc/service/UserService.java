package employeetracking.aluminc.service;

import employeetracking.aluminc.exception.ApplicationException;
import employeetracking.aluminc.model.User;
import employeetracking.aluminc.repository.UserRepository;
import employeetracking.aluminc.util.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class UserService {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public User create(User user) {
        logger.debug("Creating User {} ...", user.getFullName());
        userRepository.save(user);
        logger.debug("Creating User {} ...", user.getFullName());
        return user;
    }

    public User getUser(int id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ApplicationException("User not found", ErrorCode.NOT_FOUND));
        logger.debug("Get User {} ...",  user.getFullName());
        return user;
    }

    public List<User> getAll() {
        List<User> userList = userRepository.findAll();
        return userList;
    }

    public User updateUser(User user){
        logger.debug("Updating User {} ...", user.getFullName());
        User oldUser = userRepository.findById(user.getId()).orElseThrow(() -> new ApplicationException("User not found", ErrorCode.NOT_FOUND));
        userRepository.save(user);
        logger.debug("Updating User {} ...", user.getFullName());
        return user;
    }

    public String deleteUser(int id) {
        User employee = userRepository.findById(id).orElseThrow(() -> new ApplicationException("User not found", ErrorCode.NOT_FOUND));
        try {

            userRepository.delete(getUser(id));
        }
        catch (Exception DataIntegrityViolationException){
            return "must return";
        }
        return "must return";
    }

}
