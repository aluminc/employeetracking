package employeetracking.aluminc.service;

import employeetracking.aluminc.exception.ApplicationException;
import employeetracking.aluminc.model.Device;
import employeetracking.aluminc.repository.DeviceRepository;
import employeetracking.aluminc.util.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class DeviceService {

    private static Logger logger = LoggerFactory.getLogger(DeviceService.class);

    @Autowired
    private DeviceRepository deviceRepository;

    public Device create(Device device) {
        logger.debug("Creating Device {} ...", device.getCpuId());
        deviceRepository.save(device);
        logger.debug("Created Device {}", device.getCpuId());
        return device;
    }

    public Device getDevice(int id) {
        Device device = deviceRepository.findById(id).orElseThrow(() -> new ApplicationException("Device not found", ErrorCode.NOT_FOUND));
        logger.debug("Get Device {} ...", device.getCpuId());
        return device;
    }

    public int getId(String cpuId) {
        int deviceId=0;
        try {
            Device device = deviceRepository.findByCpuId(cpuId).orElseThrow(() -> new ApplicationException("Device not found", ErrorCode.NOT_FOUND));
            deviceId=device.getId();
        }catch (Exception e){}
        return deviceId;
    }

    public int getCompanyId(String cpuId) {
        int companyId=0;
        try {
            Device device = deviceRepository.findByCpuId(cpuId).orElseThrow(() -> new ApplicationException("Device not found", ErrorCode.NOT_FOUND));
            companyId=device.getCompanyId();
        }catch (Exception e){}
        return companyId;
    }


    public List<Device> getAll() {
        List<Device> deviceList = deviceRepository.findAll();
        return deviceList;
    }

    public Device updateDevice(Device device){
        logger.debug("Updating Device {} ...", device.getCpuId());
        Device oldDevice = deviceRepository.findById(device.getId()).orElseThrow(() -> new ApplicationException("Device not found", ErrorCode.NOT_FOUND));
        deviceRepository.save(device);
        logger.debug("Device Updated{} ...", device.getCpuId());
        return device;
    }

    public String deleteDevice(int id) {
        Device depot = deviceRepository.findById(id).orElseThrow(() -> new ApplicationException("Device not found", ErrorCode.NOT_FOUND));
        try {

            deviceRepository.delete(getDevice(id));
        }
        catch (Exception DataIntegrityViolationException)
        {  // logger.debug("Please Remove Groups of Depot");
            return "must return";
        }
       // logger.debug("Depot deleted {} ...", depot.toString());
        return "must return";
    }

}
