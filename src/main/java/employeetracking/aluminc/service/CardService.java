package employeetracking.aluminc.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.repository.CardRepository;
import employeetracking.aluminc.exception.ApplicationException;
import employeetracking.aluminc.util.ErrorCode;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class CardService {

    private static Logger logger = LoggerFactory.getLogger(CardService.class);

    @Autowired
    private CardRepository cardRepository;

    public Card create(Card card) {
       logger.debug("Creating Card {} ...", card.getRfId());
        cardRepository.save(card);
        logger.debug("Created Card {}", card.getRfId());
        return card;
    }

    public Card getCard(int id) {
        Card card = cardRepository.findById(id).orElseThrow(() -> new ApplicationException("Card not found", ErrorCode.NOT_FOUND));
        logger.debug("Get Card {} ...", card.getRfId());
        return card;
    }

    public int getId(String rfId) {
       int cardId=0;
        try {
            Card card = cardRepository.findByRfId(rfId).orElseThrow(() -> new ApplicationException("Card not found", ErrorCode.NOT_FOUND));
            cardId=card.getId();
        }catch (Exception e){}
        return cardId;
    }

    public List<Card> getAll() {
        List<Card> cardList = cardRepository.findAll();
        return cardList;
    }

    public Card updateCard(Card card){
        logger.debug("Updating Card {} ...", card.getRfId());
        System.out.println(card);
        Card oldCard = cardRepository.findById(card.getId()).orElseThrow(() -> new ApplicationException("Card not found", ErrorCode.NOT_FOUND));
        cardRepository.save(card);
        logger.debug("Card Updated{} ...", card.getRfId());
        return card;
    }

    public String deleteCard(int id) {
        Card card = cardRepository.findById(id).orElseThrow(() -> new ApplicationException("Card not found", ErrorCode.NOT_FOUND));
        try {

            cardRepository.delete(getCard(id));
        }
        catch (Exception DataIntegrityViolationException)
        {
            return "must return";
        }
        return "must return";
    }

}
