package employeetracking.aluminc.repository;

import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.model.Device;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface DeviceRepository extends CrudRepository<Device, Long> {
    Optional<Device> findById(int id);
    Optional<Device> findByCpuId(String cpuId);
    List<Device> findAll();
}
