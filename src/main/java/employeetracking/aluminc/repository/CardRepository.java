package employeetracking.aluminc.repository;

import employeetracking.aluminc.model.Card;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

//public interface CardRepository extends JpaRepository<Card, Long> {}
public interface CardRepository extends CrudRepository<Card, Long> {
    Optional<Card> findById(int id);
    Optional<Card> findByRfId(String rfId);
    List<Card> findAll();
}
