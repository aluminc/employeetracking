package employeetracking.aluminc.repository;

import employeetracking.aluminc.model.Transaction;
import employeetracking.aluminc.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by renan on 01/06/17.
 */
@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findById(int id);
    List<User> findAll();
}
