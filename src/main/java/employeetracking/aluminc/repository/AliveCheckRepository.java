package employeetracking.aluminc.repository;

import employeetracking.aluminc.model.AliveCheck;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface AliveCheckRepository extends CrudRepository<AliveCheck, Long> {
    Optional<AliveCheck> findById(int id);
    List<AliveCheck> findAll();
}
