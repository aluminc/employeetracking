package employeetracking.aluminc.repository;

import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.model.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Optional<Employee> findById(int id);
    List<Employee> findAllByCompanyId(int companyId);
    List<Employee> findAll();
}
