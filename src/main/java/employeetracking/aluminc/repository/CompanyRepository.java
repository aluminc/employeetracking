package employeetracking.aluminc.repository;

import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.model.Company;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CompanyRepository extends CrudRepository<Company, Long> {
    Optional<Company> findById(int id);
    List<Company> findAll();
}
