package employeetracking.aluminc.repository;

import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.model.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
    Optional<Transaction> findById(int id);
    List<Transaction> findAll();
}
