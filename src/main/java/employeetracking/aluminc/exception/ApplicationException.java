package employeetracking.aluminc.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import employeetracking.aluminc.util.ErrorCode;
import employeetracking.aluminc.util.Util;


/**
 * ApplicationException holds information about an error that occurred in a way that distinguishes between what kind
 * of error the client should see and what a debugger/logger should see.
 * <p>
 * The Throwable argument to the constructors is for logging/debugging purposes only. It is not revealed to the client.
 * <p>
 * The message and errorCode arguments to the constructor describe the error sent to the client. The error code is one
 * of the fixed values from ErrorCode that the client should use to decide what to do. The message is meant for human
 * readers only, and is useful for debugging. The client code shouldn't, and shouldn't need to, use it to decide what
 * to do.
 */
@JsonIgnoreProperties({"stackTrace", "suppressed", "cause", "localizedMessage"})
@JsonPropertyOrder({"message", "errorCode"})
public class ApplicationException extends RuntimeException {

    private final ErrorCode errorCode;

    /**
     * Constructor for cases where the kind of error that occurred is not known at the throw site.
     */
    public ApplicationException(Throwable cause) {
        super(cause.getMessage(), cause);
        errorCode = extractErrorCode(cause);
    }

    /**
     * Constructor for cases where the kind of error that occurred is known.
     */
    public ApplicationException(ErrorCode errorCode) {
        super();
        this.errorCode = errorCode;
    }

    /**
     * Constructor for cases where the kind of error that occurred is known along with some extra information.
     */
    public ApplicationException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Constructor for cases where the kind of error that occurred is known, and the root cause is an com.lazada.zoneservice.exception
     * thrown by some other service.
     */
    public ApplicationException(ErrorCode errorCode, Throwable cause) {
        super(null, cause);
        this.errorCode = errorCode;
    }

    /**
     * Constructor for cases where the kind of error that occurred is known along with some extra information, and
     * the root cause is an com.lazada.zoneservice.exception thrown by some other service.
     */
    public ApplicationException(String message, ErrorCode errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    private static ErrorCode extractErrorCode(Throwable exc) {
        if (exc == null) {
            return ErrorCode.UNEXPECTED_ERROR;
        }
        if (exc instanceof ApplicationException) {
            return ((ApplicationException) exc).getErrorCode();
        }
        return extractErrorCode(exc.getCause());
    }

    @Override
    public String toString() {
        try {
            return "ApplicationException" + Util.jsonString(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}


