package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.AliveCheck;
import employeetracking.aluminc.service.AliveCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/aliveCheck")
public class AliveCheckController {
	@Autowired
	private AliveCheckService aliveCheckService;

	@PostMapping
	public ResponseEntity createAliveCheck(@RequestBody AliveCheck aliveCheck){
		return new ResponseEntity(aliveCheckService.create(aliveCheck), HttpStatus.CREATED);
	}

	@GetMapping(path="/{id}")
	public ResponseEntity getAliveCheck(@PathVariable int id){
		return new ResponseEntity(aliveCheckService.getAliveCheck(id), HttpStatus.OK);
	}

	@GetMapping(path="/all")
	public ResponseEntity getAllAliveCheck(){
		List<AliveCheck> aliveCheckList = aliveCheckService.getAll();
		return new ResponseEntity(aliveCheckList, HttpStatus.OK);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity updateAliveCheck(@PathVariable int id, @RequestBody AliveCheck aliveCheck){
		aliveCheck.setId(id);
		return new ResponseEntity(aliveCheckService.updateAliveCheck(aliveCheck), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity deleteAliveCheck(@PathVariable int id){
		return new ResponseEntity( aliveCheckService.deleteAliveCheck(id),HttpStatus.OK);


	}



}
