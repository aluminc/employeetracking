package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Card;
import employeetracking.aluminc.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import java.util.List;



@RestController
@RequestMapping("/card")
public class CardController {
	@Autowired
	private CardService cardService;

	@PostMapping
	//@ApiOperation(value = "Create Depot", nickname = "createdepot")
	public ResponseEntity createCard(@RequestBody Card card){
		return new ResponseEntity(cardService.create(card), HttpStatus.CREATED);
	}

	@GetMapping(path="/{id}")
	//@ApiOperation(value = "Get Depot", nickname = "getdepot")
	public ResponseEntity getCard(@PathVariable int id){
		return new ResponseEntity(cardService.getCard(id), HttpStatus.OK);
	}

	@GetMapping(path="/all")
//	@ApiOperation(value = "Get All Depot", nickname = "getalldepot")
	public ResponseEntity getAllCard(){
		List<Card> cardList = cardService.getAll();
		return new ResponseEntity(cardList, HttpStatus.OK);
	}

	@PutMapping("/{id}")
//	@ApiOperation(value = "Update Depot", nickname = "updatedepot")
	public ResponseEntity updateCard(@PathVariable int id, @RequestBody Card card){
	card.setId(id);
	return new ResponseEntity(cardService.updateCard(card), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
//	@ApiOperation(value = "Delete Depot", nickname = "deletedepot")
	public ResponseEntity deleteCard(@PathVariable int id){
		return new ResponseEntity( cardService.deleteCard(id),HttpStatus.OK);

	}



}
