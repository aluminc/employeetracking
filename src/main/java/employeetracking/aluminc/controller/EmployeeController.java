package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Employee;
import employeetracking.aluminc.model.EmployeeIdRfId;
import employeetracking.aluminc.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/employee")
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@PostMapping
	public ResponseEntity createEmployee(@RequestBody Employee employee){
		return new ResponseEntity(employeeService.create(employee), HttpStatus.CREATED);
	}

	@GetMapping(path="/{id}")
	public ResponseEntity getEmployee(@PathVariable int id){
		return new ResponseEntity(employeeService.getEmployee(id), HttpStatus.OK);
	}

	@GetMapping(path="/all")
	public ResponseEntity getAllEmployee(){
		List<Employee> employeeList = employeeService.getAll();
		return new ResponseEntity(employeeList, HttpStatus.OK);
	}

	@GetMapping(path="/all/{id}")
	public ResponseEntity getAllEmployeeByDevice(@PathVariable String id){
		List<EmployeeIdRfId> employeeList = employeeService.getAllByDevice(id);
		return new ResponseEntity(employeeList, HttpStatus.OK);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity updateEmployee(@PathVariable int id, @RequestBody Employee employee){
		employee.setId(id);
		return new ResponseEntity(employeeService.updateEmployee(employee), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity deleteEmployee(@PathVariable int id){
		return new ResponseEntity( employeeService.deleteEmployee(id),HttpStatus.OK);

	}



}
