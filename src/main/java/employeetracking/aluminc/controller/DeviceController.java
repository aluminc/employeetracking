package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Device;
import employeetracking.aluminc.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/device")
public class DeviceController {
	@Autowired
	private DeviceService deviceService;

	@PostMapping
	//@ApiOperation(value = "Create Depot", nickname = "createdepot")
	public ResponseEntity createDevice(@RequestBody Device device){
		return new ResponseEntity(deviceService.create(device), HttpStatus.CREATED);
	}

	@GetMapping(path="/{id}")
	//@ApiOperation(value = "Get Depot", nickname = "getdepot")
	public ResponseEntity getDevice(@PathVariable int id){
		return new ResponseEntity(deviceService.getDevice(id), HttpStatus.OK);
	}

	@GetMapping(path="/all")
//	@ApiOperation(value = "Get All Depot", nickname = "getalldepot")
	public ResponseEntity getAllDevice(){
		List<Device> deviceList = deviceService.getAll();
		return new ResponseEntity(deviceList, HttpStatus.OK);
	}

	@PutMapping(value = "/{id}")
//	@ApiOperation(value = "Update Depot", nickname = "updatedepot")
	public ResponseEntity updateDevice(@PathVariable int id, @RequestBody Device device){
		device.setId(id);
		return new ResponseEntity(deviceService.updateDevice(device), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
//	@ApiOperation(value = "Delete Depot", nickname = "deletedepot")
	public ResponseEntity deleteDevice(@PathVariable int id){
		return new ResponseEntity( deviceService.deleteDevice(id),HttpStatus.OK);


	}



}
