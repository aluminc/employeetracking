package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.User;
import employeetracking.aluminc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity createUser(@RequestBody User user){
		return new ResponseEntity(userService.create(user), HttpStatus.CREATED);
	}

	@GetMapping(path="/{id}")
	public ResponseEntity getCard(@PathVariable int id){
		return new ResponseEntity(userService.getUser(id), HttpStatus.OK);
	}

	@GetMapping(path="/all")
	public ResponseEntity ggetAllUser(){
		List<User> userList = userService.getAll();
		return new ResponseEntity(userList, HttpStatus.OK);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity updateUser(@PathVariable int id, @RequestBody User user){
		user.setId(id);
		return new ResponseEntity(userService.updateUser(user), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity deleteUser(@PathVariable int id){
		return new ResponseEntity( userService.deleteUser(id),HttpStatus.OK);

	}



}
