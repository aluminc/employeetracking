package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Company;
import employeetracking.aluminc.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/company")
public class CompanyController {
	@Autowired
	private CompanyService companyService;

	@PostMapping
	public ResponseEntity createCompany(@RequestBody Company company){
		return new ResponseEntity(companyService.create(company), HttpStatus.CREATED);
	}

	@GetMapping(path="/{id}")
	public ResponseEntity getCompoany(@PathVariable int id){
		return new ResponseEntity(companyService.getCompany(id), HttpStatus.OK);
	}

	@GetMapping(path="/all")
	public ResponseEntity getAllCompany(){
		List<Company> companyList = companyService.getAll();
		return new ResponseEntity(companyList, HttpStatus.OK);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity updateCompany(@PathVariable int id, @RequestBody Company company){
		company.setId(id);
		return new ResponseEntity(companyService.updateCompany(company), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity deleteCompany(@PathVariable int id){
		return new ResponseEntity( companyService.deleteCompany(id),HttpStatus.OK);

	}



}
