package employeetracking.aluminc.controller;

import employeetracking.aluminc.model.Transaction;
import employeetracking.aluminc.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping("/transaction")
public class TransactionController {
	@Autowired
	private TransactionService transactionService;

	@PostMapping
	//@ApiOperation(value = "Create Depot", nickname = "createdepot")
	public ResponseEntity createTransaction(@RequestBody Transaction transaction){
		return new ResponseEntity(transactionService.create(transaction), HttpStatus.CREATED);
	}

	@GetMapping(path="/{id}")
	//@ApiOperation(value = "Get Depot", nickname = "getdepot")
	public ResponseEntity getCard(@PathVariable int id){
		return new ResponseEntity(transactionService.getTransaction(id), HttpStatus.OK);
	}

	@GetMapping(path="/all")
//	@ApiOperation(value = "Get All Depot", nickname = "getalldepot")
	public ResponseEntity getAllTransaction(){
		List<Transaction> transactionList = transactionService.getAll();
		return new ResponseEntity(transactionList, HttpStatus.OK);
	}

	@PutMapping(value = "/{id}")
//	@ApiOperation(value = "Update Depot", nickname = "updatedepot")
	public ResponseEntity updateTransaction(@PathVariable int id, @RequestBody Transaction transaction){
		transaction.setId(id);
		return new ResponseEntity(transactionService.updateTransaction(transaction), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
//	@ApiOperation(value = "Delete Depot", nickname = "deletedepot")
	public ResponseEntity deleteTransaction(@PathVariable int id){
		return new ResponseEntity(transactionService.deleteTransaction(id),HttpStatus.OK);

	}



}

