USE  `employeetracking`;

CREATE TABLE `card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rf_id` varchar(50) NOT NULL,
  `status` varchar(45) DEFAULT '0' COMMENT '0 Disabled\n1 Enabled\n2 TicketGenerated',
  `cpu_id` varchar(255) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `email_adress` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `adress` varchar(255) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `zip_code` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpu_id` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0 disabled\n1 enabled',
  `last_log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `card_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `status` varchar(45) NOT NULL COMMENT '0 Disabled\n1 Enabled',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `cpu_id` varchar(45) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  `rf_id` varchar(45) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `transaction_type` varchar(10) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0 OnTime\n1 Late',
  `log_num` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '3' COMMENT '0 SuperAdmin\n1 Admin\n2 Agent\n3 User',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `alive_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `cpu_id` varchar(45) DEFAULT NULL,
  `cpu_temp` varchar(10) DEFAULT '',
  `amb_temp` varchar(10) DEFAULT '',
  `date_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT 0 ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
